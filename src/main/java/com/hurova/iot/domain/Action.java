package com.hurova.iot.domain;


import com.hurova.iot.beacon.model.Beacon;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Action {

    private Long id;
    private Beacon beacon;
    private LocalDateTime submitTime;
    // in seconds
    private Long totalTime;
    private Gender gender;
    private Short age;
}