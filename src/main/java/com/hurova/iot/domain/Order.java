package com.hurova.iot.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Order {
    private Long id;
    private Action action;
    private Product product;
    private int count;
}
