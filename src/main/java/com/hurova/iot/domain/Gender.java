package com.hurova.iot.domain;

import lombok.Getter;

@Getter
public enum Gender {
    WOMEN("w"), MEN("m"), CHILD("ch");

    private String value;

    Gender(String value) {
        this.value = value;
    }
}
