package com.hurova.iot.domain;

import com.hurova.iot.beacon.model.Beacon;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class Point {
    private Long id;
    private Beacon beacon;
    private LocalDateTime submitTime;
    private String distances;

}
