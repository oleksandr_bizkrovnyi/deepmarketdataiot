package com.hurova.iot.utils;

import java.util.Map;

import static java.util.stream.Collectors.joining;

public class StringUtils {

    public static String makeStringFromMap(Map map, String delimiter){
       return (String) map.entrySet()
                .stream()
                .map(Object::toString)
                .collect(joining(delimiter));
    }
}
