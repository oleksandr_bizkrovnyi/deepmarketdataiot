package com.hurova.iot.utils;

import com.hurova.iot.repository.TokenAuthRepository;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class HttpUtils {

    public static HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(TokenAuthRepository.authToken);
        return headers;
    }
}
