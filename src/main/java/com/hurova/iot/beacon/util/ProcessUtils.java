package com.hurova.iot.beacon.util;

import com.hurova.iot.beacon.model.Tuple;
import lombok.extern.java.Log;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;

@Component
@Log
public class ProcessUtils {

    public Tuple<Process,BufferedReader> runScript(String scriptName) {
        try {
            Process p = Runtime.getRuntime().exec(scriptName);
            log.info("Python script successful run");
            return new Tuple<>(p, new BufferedReader(new InputStreamReader(p.getInputStream())));
        } catch (IOException e) {
            log.log(Level.SEVERE,"Unable to start the process", e);
           throw new IllegalArgumentException(e);
        }
    }

}
