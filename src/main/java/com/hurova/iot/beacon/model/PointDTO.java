package com.hurova.iot.beacon.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class PointDTO {

    private String beaconUuid;
    private LocalDateTime submitTime;
    private String distances;

}
