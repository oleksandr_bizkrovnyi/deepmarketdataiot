package com.hurova.iot.beacon.model;

import java.util.Objects;

public class Beacon {

    private Long id;
    private String uuid;
    private State state;
    private int rssi;
    private int ttx;

    public String getUuid() {
        return uuid;
    }

    public Beacon setUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public State getState() {
        return state;
    }

    public Beacon setState(State state) {
        this.state = state;
        return this;
    }

    public int getRssi() {
        return rssi;
    }

    public Beacon setRssi(int rssi) {
        this.rssi = rssi;
        return this;
    }

    public int getTtx() {
        return ttx;
    }

    public Beacon setTtx(int ttx) {
        this.ttx = ttx;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Beacon beacon = (Beacon) o;
        return uuid.equals(beacon.uuid);
    }

    public Long getId() {
        return id;
    }

    public Beacon setId(Long id) {
        this.id = id;
        return this;
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }

    @Override
    public String toString() {
        return "Beacon{" +
                "id=" + id +
                ", uuid='" + uuid + '\'' +
                ", state=" + state +
                ", rssi=" + rssi +
                ", ttx=" + ttx +
                '}';
    }
}
