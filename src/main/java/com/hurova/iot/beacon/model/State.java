package com.hurova.iot.beacon.model;

public enum State {
    ACTIVE, DISABLED
}
