package com.hurova.iot.beacon.management.provider;

import com.hurova.iot.beacon.management.cos.CosTheoryImpl;
import com.hurova.iot.beacon.model.Beacon;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Qualifier("cosTheoryProvider")
public class CosTheoryBeaconDataProvider implements BeaconDataProvider {

    @Override
    public Map<Long, Double> getDistance(Beacon beacon) {
      return new CosTheoryImpl().calculateDistances(beacon);
    }
}
