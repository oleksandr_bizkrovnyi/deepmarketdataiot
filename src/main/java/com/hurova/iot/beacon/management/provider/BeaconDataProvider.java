package com.hurova.iot.beacon.management.provider;

import com.hurova.iot.beacon.model.Beacon;

import java.util.Map;

public interface BeaconDataProvider {
    Map<Long, Double> getDistance(Beacon beacon);
}
