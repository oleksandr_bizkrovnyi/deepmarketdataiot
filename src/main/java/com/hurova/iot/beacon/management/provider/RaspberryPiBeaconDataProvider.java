package com.hurova.iot.beacon.management.provider;

import com.hurova.iot.beacon.model.Beacon;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class RaspberryPiBeaconDataProvider implements BeaconDataProvider {

    @Override
    public Map<Long, Double> getDistance(Beacon beacon) {
        throw new UnsupportedOperationException("In next releases");
    }
}
