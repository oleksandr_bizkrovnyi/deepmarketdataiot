package com.hurova.iot.beacon.management.cos;

import com.hurova.iot.beacon.model.Beacon;
import com.hurova.iot.beacon.model.Tuple;
import com.hurova.iot.repository.RaspberryPiRepository;
import lombok.var;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class CosTheoryImpl {

    Iterator<Long> iterator = RaspberryPiRepository.raspberriesLiveStorage.keySet().iterator();


    public Map<Long, Double> calculateDistances(Beacon beacon) {
        Map<Long, Double> distances = new LinkedHashMap<>();
        double beaconToPiDistance = new BigDecimal(calculateAccuracy(beacon.getTtx(), beacon.getRssi())).setScale(2, RoundingMode.HALF_UP).doubleValue();

        long idFirstRasp = iterator.next();
        long idSecondRasp = iterator.next();
        long idThirdRasp = iterator.next();

        distances.put(idFirstRasp, beaconToPiDistance);
        double distanceBetweenFirstAndSecondRasp = RaspberryPiRepository.raspberriesDistances.get(new Tuple<>(idFirstRasp, idSecondRasp));
        distances.put(idSecondRasp, generateDistanceClosestSideToBeacon(beaconToPiDistance, distanceBetweenFirstAndSecondRasp));
        distances.put(idThirdRasp, calculateThirdSideBeaconToPi(RaspberryPiRepository.raspberriesDistances, distances));
        return distances;
    }


    private Double generateDistanceClosestSideToBeacon(double distanceToBeacon, double distanceToClosestPi) {
        double randomSecondDistance = ThreadLocalRandom.current().nextDouble(distanceToClosestPi - distanceToBeacon, (distanceToBeacon + distanceToClosestPi) + .01);
        return new BigDecimal(randomSecondDistance).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }


    private static double calculateAccuracy(int txPower, double rssi) {
        if (rssi == 0) {
            return -1.0;
        }
        double ratio = rssi * 1.0 / txPower;
        if (ratio < 1.0) {
            return Math.pow(ratio, 10);
        } else {
            return (0.89976) * Math.pow(ratio, 7.7095) + 0.111;
        }
    }

    private double getCosByThreeSides(double firstSide, double secondSide, double thirdSide) {
        return (Math.pow(firstSide, 2) + Math.pow(secondSide, 2) - Math.pow(thirdSide, 2)) / (2 * firstSide * secondSide);
    }

    private double getAngleByCos(double cos) {
        return (Math.acos(cos) * 180 / Math.PI);
    }

    private double getCosByAngle(double angle) {
        return Math.cos(Math.toRadians(angle));
    }

    private double calculateThirdSideBeaconToPi(Map<Tuple<Long, Long>, Double> piDistances, Map<Long, Double> distancesToBeacon) {
        Iterator<Map.Entry<Tuple<Long, Long>, Double>> iterator = piDistances.entrySet().iterator();
        double AB = iterator.next().getValue();
        double BC = iterator.next().getValue();
        double AC = iterator.next().getValue();
        double BD = distancesToBeacon.entrySet().stream().findFirst().get().getValue();
        double DC = distancesToBeacon.entrySet().stream().skip(1).findFirst().get().getValue();
        double cosABC = getCosByThreeSides(AB, BC, AC);
        double cosDBC = getCosByThreeSides(BD, BC, DC);
        double ABDAngle = getAngleByCos(cosABC) - getAngleByCos(cosDBC);
        double thirdSide = Math.sqrt(Math.pow(AB, 2) + Math.pow(BD, 2) - 2 * AB * BD * getCosByAngle(ABDAngle));
        if(!Double.isNaN(thirdSide)){
            return new BigDecimal(thirdSide).setScale(2, RoundingMode.HALF_UP).doubleValue();
        }
        return thirdSide;
    }
}
