package com.hurova.iot.beacon.management;

import com.google.gson.Gson;
import com.hurova.iot.beacon.management.provider.BeaconDataProvider;
import com.hurova.iot.beacon.model.Beacon;
import com.hurova.iot.beacon.model.Tuple;
import com.hurova.iot.controllers.BeaconController;
import com.hurova.iot.repository.BeaconStorage;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;

import static com.hurova.iot.beacon.model.State.DISABLED;

@Component
@ConfigurationProperties(prefix = "beacon")
@EnableConfigurationProperties
public class BeaconManagement {

    @Getter
    @Setter
    private Integer timeToSendData;

    private final BeaconController beaconController;

    private final BeaconDataProvider beaconDataProvider;

    @Autowired
    public BeaconManagement(@Qualifier("cosTheoryProvider") BeaconDataProvider beaconDataProvider, BeaconController beaconController) {
        this.beaconDataProvider = beaconDataProvider;
        this.beaconController = beaconController;
    }

    public void observeBeacons(BufferedReader raspberryOutput) {
        Map<String, Tuple<Beacon, Integer>> beaconLiveStorage = BeaconStorage.beaconLiveStorage;

        raspberryOutput.lines()
                .map(convertBeacon)
                .filter(filterDisabled)
                .forEach(item -> {
                    if (isSameBeacon(item)) {
                        int repeatCount = beaconLiveStorage.get(item.getUuid()).y;
                        if (isSameRssi(item)) {
                            sendOrUpdateBeaconInStorage(item, beaconLiveStorage, repeatCount);
                        } else {
                            setFullBeaconToStorage(item, beaconLiveStorage);
                        }
                    }
                });
    }

    private Predicate<Beacon> filterDisabled = (beacon) -> beacon != null && BeaconStorage.beaconLiveStorage.get(beacon.getUuid()).x.getState() != DISABLED;

    private boolean isSameBeacon(Beacon beacon) {
        return BeaconStorage.beaconLiveStorage.values().stream()
                .anyMatch(item -> item.x.getUuid().equals(beacon.getUuid()));
    }

    private boolean isSameRssi(Beacon beacon) {
        return BeaconStorage.beaconLiveStorage.values().stream()
                .anyMatch(item -> (
                        ((item.x.getRssi() + 10) >= beacon.getRssi())
                                && ((item.x.getRssi() - 10) <= beacon.getRssi())));

    }

    private void setFullBeaconToStorage(Beacon beacon, Map<String, Tuple<Beacon, Integer>> beaconLiveStorage) {
        Beacon updatedBeacon = beaconLiveStorage.get(beacon.getUuid()).x.setRssi(beacon.getRssi());
        beaconLiveStorage.put(beacon.getUuid(), new Tuple<>(updatedBeacon, 0));
    }

    private void sendOrUpdateBeaconInStorage(Beacon beacon, Map<String, Tuple<Beacon, Integer>> beaconLiveStorage, int repeatCount) {
        Beacon storageBeacon = beaconLiveStorage.get(beacon.getUuid()).x;

        if (beaconLiveStorage.get(beacon.getUuid()).y >= timeToSendData) {
            beaconController.provideBeaconInfo(beaconDataProvider.getDistance(storageBeacon), storageBeacon.getUuid());
            beaconLiveStorage.put(beacon.getUuid(), new Tuple<>(storageBeacon, 0));
        } else {
            beaconLiveStorage.put(beacon.getUuid(), new Tuple<>(storageBeacon, ++repeatCount));
        }
    }


    private Function<String, Beacon> convertBeacon = (stringValue) -> new Gson().fromJson(stringValue, Beacon.class);
}
