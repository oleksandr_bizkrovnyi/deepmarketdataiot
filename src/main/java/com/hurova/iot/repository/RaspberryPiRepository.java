package com.hurova.iot.repository;

import com.hurova.iot.beacon.model.Tuple;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class RaspberryPiRepository {
    // pi id, active
    public static Map<Long, Boolean> raspberriesLiveStorage = new LinkedHashMap<>();
    // <first pi, second pi>, distance
    public static Map<Tuple<Long, Long>, Double> raspberriesDistances = new HashMap<>();
}
