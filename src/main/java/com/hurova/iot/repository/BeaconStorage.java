package com.hurova.iot.repository;

import com.hurova.iot.beacon.model.Beacon;
import com.hurova.iot.beacon.model.Tuple;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

public class BeaconStorage {
    // uuid, beacon, repeat count
    public static Map<String, Tuple<Beacon,Integer>> beaconLiveStorage = new HashMap<>();
}
