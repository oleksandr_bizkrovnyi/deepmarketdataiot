package com.hurova.iot.config.scheduler.job;

import com.hurova.iot.beacon.management.BeaconManagement;
import com.hurova.iot.beacon.model.Beacon;
import com.hurova.iot.beacon.model.Tuple;
import com.hurova.iot.beacon.util.ProcessUtils;
import com.hurova.iot.config.ApplicationContextProvider;
import com.hurova.iot.domain.Action;
import com.hurova.iot.domain.Gender;
import com.hurova.iot.domain.Order;
import com.hurova.iot.repository.BeaconStorage;
import com.hurova.iot.repository.ProductRepository;
import lombok.extern.java.Log;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.hurova.iot.utils.HttpUtils.getHeaders;
import static com.hurova.iot.utils.URLConstants.BASE_URL;

@Log
public class StartActionJob implements Job {

    private static final Integer WAIT_PERIOD = 30_000;


    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        Action action = simulateStartActionByBeacon(BeaconStorage.beaconLiveStorage.values().stream().findFirst().get().x);
        action = makeObserving(action);
//         TODO make this by camera
        action = resolveAgeAndGender(action);
        action = saveAction(action);
        log.log(Level.INFO, "The Action was stored: {0}", action.toString());
        List<Order> orderList = saveOrder(createOrder(action));
        log.log(Level.INFO, "The Order with products was stored{0}", orderList.toString());
    }

    private Action simulateStartActionByBeacon(Beacon beacon) {
        Action action = new Action();
        action.setBeacon(beacon);
        action = saveAction(action);
        log.log(Level.INFO, "The Action is started by beacon {0}", beacon.toString());
        return action;
    }

    private Action makeObserving(Action action) {
        long start = System.currentTimeMillis();

       Thread observingThread = runObservingThread();
       observingThread.start();

        log.log(Level.INFO, "The Beacon observing is started");
        try {
            log.log(Level.INFO, "Continue observing in {0} second", WAIT_PERIOD / 1000);
            Thread.sleep(WAIT_PERIOD);
        } catch (InterruptedException e) {

        }
        log.log(Level.INFO, "Stop observing");
        observingThread.interrupt();
        long finish = System.currentTimeMillis();

        action.setSubmitTime(LocalDateTime.now());
        action.setTotalTime((finish - start) / 1000);
        return action;
    }

    private Action resolveAgeAndGender(Action action) {
        action.setAge((short) (new Random().nextInt(40) + 10));

        if (action.getAge() % 2 == 0) {
            action.setGender(Gender.MEN);
        } else {
            action.setGender(Gender.WOMEN);
        }
        return action;
    }

    private Thread runObservingThread() {
        Thread obserThread = new Thread(() -> {
            Process observeProcess = runObservingScript();
            long startProcessTime = System.currentTimeMillis();
            while (System.currentTimeMillis() - startProcessTime >= WAIT_PERIOD - 10_000) {
                observeProcess.destroyForcibly();
            }
        });
        return obserThread;
    }

    private Process runObservingScript() {
        BeaconManagement beaconManagement = ApplicationContextProvider.getApplicationContext().getBean("beaconManagement", BeaconManagement.class);
        ProcessUtils processUtils = ApplicationContextProvider.getApplicationContext().getBean("processUtils", ProcessUtils.class);
        Tuple<Process, BufferedReader> processTuple = processUtils.runScript("sudo python /home/pi/Desktop/java_hurova/BeaconScanner.py");
        beaconManagement.observeBeacons(processTuple.y);
        log.info("Observation is started");
        return processTuple.x;
    }

    // This stuff should do another pi by invoking from this one.
    private List saveOrder(List<Order> orders) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<List<Order>> request = new HttpEntity<>(orders, getHeaders());
        ResponseEntity<List> responseEntityStr = restTemplate.postForEntity(BASE_URL + "/orders", request, List.class);
        return responseEntityStr.getBody();
    }

    private Action saveAction(Action action) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Action> request = new HttpEntity<>(action, getHeaders());
        ResponseEntity<Action> responseEntityStr = restTemplate.postForEntity(BASE_URL + "/action/", request, Action.class);
        return responseEntityStr.getBody();
    }

    private List<Order> createOrder(Action action) {
        int randomBound = new Random().nextInt(12) + 1;
        return IntStream.rangeClosed(1, randomBound).mapToObj(countProducts -> {
            Order order = new Order();
            order.setAction(action);
            order.setProduct(ProductRepository.products.get(ThreadLocalRandom.current().nextInt(ProductRepository.products.size())));
            order.setCount(new Random().nextInt(12));
            return order;
        }).collect(Collectors.toList());
    }

}
