package com.hurova.iot.config.scheduler;

import org.quartz.JobDetail;
import org.quartz.Trigger;

public class QuartzTuple {
    private JobDetail job;
    private Trigger trigger;


    public JobDetail getJob() {
        return job;
    }

    public QuartzTuple setJob(JobDetail job) {
        this.job = job;

        return this;
    }

    public Trigger getTrigger() {
        return trigger;
    }

    public QuartzTuple setTrigger(Trigger trigger) {
        this.trigger = trigger;

        return this;
    }
}