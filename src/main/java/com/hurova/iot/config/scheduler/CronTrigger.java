package com.hurova.iot.config.scheduler;

import org.quartz.CronScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

public class CronTrigger {
    public static Trigger getCronTrigger(String cronMask) {
        return TriggerBuilder
                .newTrigger()
                .withIdentity(CronTrigger.class.getSimpleName(), "Job")
                .withSchedule(CronScheduleBuilder.cronSchedule(cronMask))
                .build();
    }
}