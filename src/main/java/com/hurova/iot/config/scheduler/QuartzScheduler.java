package com.hurova.iot.config.scheduler;

import com.hurova.iot.config.scheduler.job.StartActionJob;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashSet;
import java.util.Set;

public class QuartzScheduler {

    private static Set<QuartzTuple> setJobsAndTriggers;
    private static Logger logger = LoggerFactory.getLogger(QuartzScheduler.class);
    private static QuartzScheduler quartzScheduleInstance;

    private QuartzScheduler() {
        try {
            //Retrieve the scheduler from schedule factory
            Scheduler scheduler = new StdSchedulerFactory().getScheduler();
            // Run the scheduler
            scheduler.start();
            setJobAndTriggers(scheduler, setJobsAndTriggers);
        } catch (Exception e) {
            logger.error("The scheduler cannot start: {}", e);
        }
    }

    public static QuartzScheduler getInstance() {
        if (quartzScheduleInstance == null) {
            quartzScheduleInstance = new QuartzScheduler();
        }
        return quartzScheduleInstance;
    }

    private Scheduler setJobAndTriggers(Scheduler scheduler, Set<QuartzTuple> setJobsAndTriggers) {
        setJobsAndTriggers.forEach(item -> {
            try {
                // Schedule a job with JobDetail and Trigger
                scheduler.scheduleJob(item.getJob(), item.getTrigger());
            } catch (SchedulerException e) {
                logger.error("Cannot schedule the job: {}", e);
            }
        });
        return scheduler;
    }

    public static Class<QuartzScheduler> setJobsAndTriggers(Set<QuartzTuple> setJobsAndTriggers) {
        QuartzScheduler.setJobsAndTriggers = setJobsAndTriggers;

        return QuartzScheduler.class;
    }

    public static Set<QuartzTuple> getQuartzTuple() {
        Set<QuartzTuple> quartzTuples = new LinkedHashSet<>();

        JobDetail job = JobBuilder.newJob(StartActionJob.class)
                .withIdentity("Starting action process.Is invoked every minute", "Job").build();

        quartzTuples.add(new QuartzTuple()
                .setJob(job)
                //Seconds, Minutes, Hour, Day of the month, Month, Day of the week, Year (optional)
                .setTrigger(CronTrigger.getCronTrigger("0 * * ? * *")));

        return quartzTuples;
    }
}