package com.hurova.iot.config;

import com.hurova.iot.beacon.model.Beacon;
import com.hurova.iot.beacon.model.State;
import com.hurova.iot.beacon.model.Tuple;
import com.hurova.iot.config.scheduler.QuartzScheduler;
import com.hurova.iot.controllers.BeaconController;
import com.hurova.iot.domain.Product;
import com.hurova.iot.repository.BeaconStorage;
import com.hurova.iot.repository.ProductRepository;
import com.hurova.iot.repository.RaspberryPiRepository;
import com.hurova.iot.utils.StringUtils;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.logging.Level;

import static com.hurova.iot.repository.RaspberryPiRepository.raspberriesDistances;

@Configuration
@Log
public class InitData {

    public static void init() {
        Beacon beacon = new Beacon().setId(1L).setTtx(-59).setState(State.ACTIVE).setUuid("e2c56db5-dffb-48d2-b060-d0f5a71096e0");
        BeaconStorage.beaconLiveStorage.put("e2c56db5-dffb-48d2-b060-d0f5a71096e0", new Tuple<>(beacon, 0));
        log.log(Level.INFO, "Received following active beacons {0}", StringUtils.makeStringFromMap(BeaconStorage.beaconLiveStorage, System.lineSeparator()));
        retrieveActiveRaspberryPi();
        sendRaspberriesDistanceEachOther();
        retrieveProductList();
        QuartzScheduler.setJobsAndTriggers(QuartzScheduler.getQuartzTuple());
        QuartzScheduler.getInstance();

    }


    private static void retrieveActiveRaspberryPi() {
        RaspberryPiRepository.raspberriesLiveStorage.put(1L, true);
        RaspberryPiRepository.raspberriesLiveStorage.put(2L, true);
        RaspberryPiRepository.raspberriesLiveStorage.put(3L, true);
        log.log(Level.INFO, "Pi received: {0}", StringUtils.makeStringFromMap(RaspberryPiRepository.raspberriesLiveStorage, System.lineSeparator()));
    }


    /**
     * This is used to set the distances between raspberries
     * In real life this is made by power of bluetooth signal
     */
    private static void sendRaspberriesDistanceEachOther() {
        raspberriesDistances.put(new Tuple<>(1L, 2L), 4.);
        raspberriesDistances.put(new Tuple<>(2L, 3L), 4.);
        raspberriesDistances.put(new Tuple<>(3L, 1L), 4.);

        log.log(Level.INFO, "Hardcoded distance from first raspberryPi to another: {0}", StringUtils.makeStringFromMap(raspberriesDistances, System.lineSeparator()));
        log.info("Send one distance and receive three distances");
        log.info("Set received distances to the map");
        log.log(Level.INFO, "Full Distances Map: {0}", StringUtils.makeStringFromMap(raspberriesDistances, System.lineSeparator()));
    }

    private static void retrieveProductList() {
        Product bread = new Product();
        bread.setId(1L);
        Product sweets = new Product();
        sweets.setId(2L);
        Product meet = new Product();
        meet.setId(3L);
        Product tomato = new Product();
        tomato.setId(4L);
        Product avocado = new Product();
        avocado.setId(5L);
        ProductRepository.products.add(bread);
        ProductRepository.products.add(sweets);
        ProductRepository.products.add(meet);
        ProductRepository.products.add(tomato);
        ProductRepository.products.add(avocado);
    }

}
