package com.hurova.iot.controllers;

import com.google.gson.Gson;
import com.hurova.iot.beacon.model.Beacon;
import com.hurova.iot.beacon.model.PointDTO;
import com.hurova.iot.beacon.model.Tuple;
import com.hurova.iot.repository.BeaconStorage;
import lombok.extern.java.Log;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.Map;

import static com.hurova.iot.utils.HttpUtils.getHeaders;
import static com.hurova.iot.utils.StringUtils.makeStringFromMap;
import static com.hurova.iot.utils.URLConstants.BASE_URL;

@Controller
@RequestMapping("beacon/")
@Log
public class BeaconController {

    @PostMapping("/set")
    /**
     * This method is used to change state of beacon in beaconStorage
     */
    public ResponseEntity setStateBeacon(@RequestBody Beacon beaconWithChangedState) {
        BeaconStorage.beaconLiveStorage.put(beaconWithChangedState.getUuid(), new Tuple<>(beaconWithChangedState, 0));
        return ResponseEntity.ok().build();
    }

    /**
     * This method is used to provide data about distances
     *
     * @param beaconPiDistances Long -- Pi id, Integer -- distance
     * @param uuid              of beacon, what distances are provided
     * @return
     */
    public void provideBeaconInfo(Map<Long, Double> beaconPiDistances, String uuid) {
        if (!beaconPiDistances.values().contains(Double.NaN)) {
            PointDTO point = new PointDTO();
            point.setBeaconUuid(uuid);
            point.setSubmitTime(LocalDateTime.now());
            point.setDistances(new Gson().toJson(beaconPiDistances));

            RestTemplate restTemplate = new RestTemplate();
            HttpEntity request = new HttpEntity(point, getHeaders());
            ResponseEntity responseEntityStr = restTemplate.postForEntity(BASE_URL + "/points/", request, PointDTO.class);
            log.info("Send beacon data" + makeStringFromMap(beaconPiDistances, System.lineSeparator()));
        }
    }

}
