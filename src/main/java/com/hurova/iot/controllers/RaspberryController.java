package com.hurova.iot.controllers;

import lombok.extern.java.Log;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * This controller is used to manage Pi`s configurations.
 */
@Controller
@RequestMapping("raspberry/")
@Log
public class RaspberryController {
    @GetMapping(path = "/stop")
    public void stopApp(){
        System.exit(0);
    }
}
