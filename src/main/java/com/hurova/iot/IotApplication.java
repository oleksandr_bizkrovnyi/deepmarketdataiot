package com.hurova.iot;

import com.hurova.iot.beacon.management.BeaconManagement;
import com.hurova.iot.beacon.util.ProcessUtils;
import com.hurova.iot.config.ApplicationContextProvider;
import com.hurova.iot.config.InitData;
import com.hurova.iot.controllers.BeaconController;
import com.hurova.iot.domain.AuthRequestDTO;
import com.hurova.iot.repository.TokenAuthRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;

import static com.hurova.iot.utils.URLConstants.BASE_URL;

@SpringBootApplication
@EnableConfigurationProperties(BeaconManagement.class)
@Log
public class IotApplication {


	public static void main(String[] args) throws IOException {
		SpringApplication.run(IotApplication.class, args);
        TokenAuthRepository.authToken = login();
        ProcessUtils processUtils = ApplicationContextProvider.getApplicationContext().getBean("processUtils", ProcessUtils.class);
        processUtils.runScript("python /home/pi/Desktop/global.py 8090 hurovaiot.pagekite.me");
        log.log(Level.INFO, "Pi successfully logged with and recieve token: {0}", TokenAuthRepository.authToken);
		InitData.init();
	}

    private static String login() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AuthRequestDTO> request = new HttpEntity<>(new AuthRequestDTO("iot", "iot"), headers);

        ResponseEntity<Map> responseEntityStr = restTemplate.postForEntity(BASE_URL + "/auth/login", request, Map.class);
        return (String) responseEntityStr.getBody().get("accessToken");
    }
}
